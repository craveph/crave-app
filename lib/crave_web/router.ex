defmodule CraveWeb.Router do
  use CraveWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", CraveWeb do
    pipe_through :api

    get "/explore", CraveController, :explore
    get "/category", CraveController, :category
  end
end
