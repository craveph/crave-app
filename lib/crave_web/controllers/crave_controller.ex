defmodule CraveWeb.CraveController do
  use CraveWeb, :controller

  def explore(conn, %{"latitude" => latitude,
                      "longitude" => longitude,
                      "limit" => limit,
                      "section" => section,
                      "open_now" => open_now,
                      "venue_photos" => venue_photos}
  ) do
    case Bees.Venue.explore(
      auth(),
      latitude,
      longitude,
      limit,
      section,
      open_now,
      venue_photos
    ) do
      {:ok, recommended} -> json conn, recommended
      {:error, error} -> json conn, error
    end
  end

  def category(conn, %{"latitude" => latitude,
                      "longitude" => longitude,
                      "category" => category}
  ) do
    case Bees.Venue.category(
      auth(),
      latitude,
      longitude,
      category
    ) do
      {:ok, venues} -> json conn, venues
      {:error, error} -> json conn, error
    end
  end

  defp auth do
    %Bees.Client{
      client_id: System.get_env("FOURSQUARE_ID"),
      client_secret: System.get_env("FOURSQUARE_SECRET")
    }
  end
end
