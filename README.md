## Purpose:

The problem we are trying to solve is making it easier to *decide where to eat.*

## Solution:

- Provide immediate suggestions of what and where to eat. Spark craving in the users through visual images and/or enticing descriptions.
- Save time and increase efficiency
- Promotes curiosity and encourages users to explore/try new things

## Existing Solutions:

- Yelp, Zomato, Munch Punch
- These solutions only present a list/library of restaurants and ratings but does not decide for the user.

## Why Make Another One:

- UX does not solve the problem

## Target Users:

- Barkadas with commitment issues
- Office employees who don't have the luxury of time to spend on planning meals
- Yuppies

## Possible business model (future)

- B2B — allow restaurants to advertise (sponsored)
- Collect and sell data
- Delivery

## Ideal Scenario:

_TODO_

## Possible Solutions:

- Show appetizing photos of food suggestions nearby
- When the photo/button `I want this` is clicked, reveal where the user can find/get this
- Suggestions are based on user's location
- Don't show photos/food of what users can't access
- Within 1km (walkable) up to 10km (short drive)

## Flow

#### MVP:

1. Show photos/text of different kinds of food items
  - Grid or list (instagram)
  - Text with enticing words (like reading a menu)
  - Full screen (desktop)
2. Swipe to next random photo (carousel style) of other nearby food —> consider Tinder style like/reject
3. When the call to action button is clicked, show list of places where to find/get that item

#### Next Possible Steps:

1. When user decides and clicks "I want", show directions/map of how to get there 
2. How was your experience at _[name of place]_ ? + share photos (inspired by Uber) ❤️
3. Filter by price, cuisine, restaurant type, food type, meal/time of the day
4. "Friends" —> log into account or Appear-in style: share a unified link among peers
5. Group votes (for friends) 👍 —> can chromecast on big screen for more fun!_ 
6. Fun Retro style voting system (5 votes)
