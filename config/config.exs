# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :crave,
  ecto_repos: [Crave.Repo]

# Configures the endpoint
config :crave, CraveWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "yucQAl0cN7Zs/OvJAbLGQUvB8SHv/MjIIH+64ussrBaEraStZU08Kw83OlazkbqL",
  render_errors: [view: CraveWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Crave.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
